package com.placelocalizer.main.json

import com.placelocalizer.main.exceptions.EmptyResponseException
import com.placelocalizer.main.exceptions.FatalParseException
import com.placelocalizer.main.exceptions.ParseException
import com.placelocalizer.main.common.Place
import groovy.json.JsonSlurper
import static java.lang.String.format

/**
 * Basic Google Places API parser class
 */
class JSONParser {
    // Google API statuses
    private final STATUS_OK = 'OK'
    private final STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR'
    private final STATUS_ZERO_RESULTS = 'ZERO_RESULTS'
    private final STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT'
    private final STATUS_REQUEST_DENIED = 'REQUEST_DENIED'
    private final STATUS_INVALID_REQUEST = 'INVALID_REQUEST'
    private final STATUS_NOT_FOUND = 'NOT_FOUND'

    /*
     * Parses Google Places API details JSON and returns nullable nextPage token
     * @param responce Google Places server details response
     * @param places List of places which would be filled with places found in server response
     * @throws FatalParseException If unresolvable error occured
     * @throws ParseException If resorvable error occured which might be fixed by original query repeat
     * @return Next page token string
     */
    public String parseJSON(String responce, List<Place> places) throws ParseException, FatalParseException {
        assert responce != null && places != null
        final PARSABLE_STATUSES = Arrays.asList(STATUS_OK)
        final EMPTY_STATUSES = Arrays.asList(STATUS_ZERO_RESULTS, STATUS_NOT_FOUND)
        final RETRY_STATUSES = Arrays.asList(STATUS_UNKNOWN_ERROR, STATUS_OVER_QUERY_LIMIT)
        if (responce == null || responce.length() == 0) {
            throw new ParseException('Empty server response')
        }
        def slurper = new JsonSlurper()
        def object = slurper.parseText(responce)
        if (!object instanceof Map) {
            throw new FatalParseException('Invalid response format')
        }
        String status = object.status
        String nextPage = object.next_page_token
        if (PARSABLE_STATUSES.contains(status)) {
            for (int i = 0; i < object.results.size; i++) {
                def newPlace = new Place(
                        object.results[i].name,
                        object.results[i].id,
                        object.results[i].geometry.location.lat,
                        object.results[i].geometry.location.lng,
                        object.results[i].icon
                )
                places.add(newPlace)
            }
        } else if (RETRY_STATUSES.contains(status)) {
            throw new ParseException(format('Invalid server status (%s)', status))
        } else if (EMPTY_STATUSES.contains(status)) {
            throw new EmptyResponseException(format('Zero locations found (%s)', status))
        } else {
            throw new FatalParseException(format('Fatal server status (%s)', status))
        }
        return nextPage
    }
}
