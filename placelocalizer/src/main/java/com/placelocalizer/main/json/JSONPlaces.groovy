package com.placelocalizer.main.json

import com.placelocalizer.main.common.PlacesOutputObject
import groovy.json.JsonBuilder

/**
 * Class to build JSON string
 * which wraps around existing PlacesOutputObject
 */
class JSONPlaces {
    private def jsonBuilder = new JsonBuilder()

    /*
     * Class constructor specifying PlacesOutputObject to wrap
     */
    public JSONPlaces(PlacesOutputObject output) {
        assert output != null
        jsonBuilder {
            status output.getStatus()
            places output.getPlaces()
            count output.getPlaces().size()
            radius output.getRadius()
        }
    }

    /*
     * Returns generated JSON string
     * @return JSON string
     */
    public String toString() {
        return jsonBuilder.toPrettyString()
    }
}
