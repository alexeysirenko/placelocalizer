package com.placelocalizer.main.common

/**
 * Class to store final places search output
 * which wraps around existing ParsePage
 */
class PlacesOutputObject {
    final def status
    final def places
    final def radius

    /*
     * Class constructor specifying ParsePage to wrap
     */
    public PlacesOutputObject(ParsePage page) {
        assert page != null
        Collections.sort(page.getPlaces(), new PlaceComparator(page.lat, page.lng))
        this.places = page.places
        this.status = page.status
        this.radius = page.radius
    }
}
