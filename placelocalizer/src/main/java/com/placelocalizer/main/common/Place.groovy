package com.placelocalizer.main.common

/**
 * Class to store data about singe place returned by
 * Google Places API
 * Contains initial place data such as name, unique id,
 * coordinates and icon URL
 */
class Place {
    private final String name
    private final String id
    private final double lat
    private final double lng
    private final String icon

    /*
     * Class constructor specifying initial arguments for a single place
     */
    public Place(String name, String id, double lat, double lng, String icon) {
        this.name = name
        this.id = id
        this.lat = lat
        this.lng = lng
        this.icon = icon
    }

    /*
     * Returns a distance to other place in coordinate units
     * @param secondPlace Place object
     * @return distance to second place
     */
    public double distanceTo(Place secondPlace) {
        assert secondPlace != null
        return distanceTo(secondPlace.getLat(), secondPlace.getLng())
    }

    /*
     * Returns a distance to specified location in coordinate units
     * @param lat location latitude
     * @param lng location longitude
     * @return distance to second place
     */
    public double distanceTo(double lat, double lng) {
        return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2))
    }

    /*
     * toString() custom implementation
     */
    public String toString() {
        return  String.format('%s ID=%s Name=%s Lat=%s Lng=%s Icon=%s', this.getClass().getName(), id, name, lat, lng, icon)
    }

    /*
     * Getters
     */
    String getName() {
        return name
    }

    String getId() {
        return id
    }

    double getLat() {
        return lat
    }

    double getLng() {
        return lng
    }

    String getIcon() {
        return icon
    }
}
