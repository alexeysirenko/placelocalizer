package com.placelocalizer.main.common

/**
 * Comparator for Place objects sorting in
 * dependency of distance to center
 */
class PlaceComparator implements Comparator<Place> {
    private final double centerLat
    private final double centerLng

    /*
     * Class constructor specifying initial center arguments
     * @param centerLat center latitude
     * @param centerLng center longitude
     */
    public PlaceComparator(double centerLat, double centerLng) {
        this.centerLat = centerLat
        this.centerLng = centerLng
    }

    public int compare(Place first, Place second) {
        double distanceToFirst = first.distanceTo(centerLat, centerLng)
        double distanceToSecond = second.distanceTo(centerLat, centerLng)
        return distanceToFirst - distanceToSecond < 0 ? -1 : 1
    }
}
