package com.placelocalizer.main.common

/*
 * Class to store rest query parse result
 * with list of places, status code, next page token
 * and inital parameters such as radius and coordinates
 * included
 */
public class ParsePage implements Cloneable {
    private final List<Place> places
    private String status = null
    private String nextPage = null
    private final int radius
    private final double lng
    private final double lat

    /*
     * Class constructor specifying initial arguments for a page result
     */
    public ParsePage(double lat, double lng, int radius) {
        this.places = new ArrayList<>()
        this.radius = radius
        this.lat = lat
        this.lng = lng
    }

    /*
     * Clone method
     */
    public ParsePage clone() throws CloneNotSupportedException {
        ParsePage clone = new ParsePage(this.lat, this.lng, this.radius)
        clone.status = this.status
        clone.nextPage = this.nextPage
        clone.places.addAll(this.places)
        return clone
    }

    /*
     * toString() custom implementation
     */
    public String toString() {
        return String.format('%s radius = %d size = %d status = %s nextPage = %s',
                this.getClass().getName(),
                radius,
                places.size(),
                status,
                nextPage)
    }

    /*
     * Getters and setters
     */

    public int getSize() {
        return places.size()
    }

    List<Place> getPlaces() {
        return places
    }

    int getRadius() {
        return radius
    }

    double getLng() {
        return lng
    }

    double getLat() {
        return lat
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    String getNextPage() {
        return nextPage
    }

    void setNextPage(String nextPage) {
        this.nextPage = nextPage
    }


}