package com.placelocalizer.main.web

import org.apache.http.HttpStatus

import static java.lang.String.format

/**
 * Basic Google Places API query manager
 */
class GoogleRESTQuerier {
    private final key;
    private final URL_BASE = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
    private final URL_PARAMS_DEFAULT = '?key=%s&sensor=true&location=%s,%s&radius=%d';
    private final URL_PARAMS_BYTOKEN = '?key=%s&sensor=true&pagetoken=%s';

    /*
     * Class constructor specifying Google API key string
     */
    public GoogleRESTQuerier(String key) {
        this.key = key
    }

    /*
     * Makes GET-query to Google server by coordinates and radius
     * and returns JSON string
     * @param lat place latitude
     * @param lng place longitude
     * @param radius radius limitation
     * @throws IOException in case of connection error occurred
     * @return JSON string
     */
    public String getPlacesJSON(lat, lng, radius) throws IOException {
        def url = new URL(URL_BASE + format(URL_PARAMS_DEFAULT, key, lat, lng, radius))
        return makeQuery(url)
    }

    /*
     * Makes GET-query to Google server next page token
     * and returns JSON string
     * @param lat place latitude
     * @param lng place longitude
     * @param radius radius limitation
     * @throws IOException in case of connection error occurred
     * @return JSON string
     */
    public String getPlacesJSON(token) throws IOException {
        def url = new URL(URL_BASE + format(URL_PARAMS_BYTOKEN, key, token))
        return makeQuery(url)
    }

    /*
     * Performs GET-query to Google server using previously formatted URL
     * and returns JSON string
     * @param url specified URL
     * @throws IOException in case of connection error occurred
     * @return JSON string
     */

    private String makeQuery(url) throws IOException {
        def connection = url.openConnection()
        connection.connect()
        if (connection.responseCode != HttpStatus.SC_OK) {
            throw new IOException(format('Invalid server response code (%d)', connection.responseCode))
        }
        return connection.content.text
    }
}
