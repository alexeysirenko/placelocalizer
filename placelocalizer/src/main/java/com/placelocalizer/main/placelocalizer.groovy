package com.placelocalizer.main

import com.placelocalizer.main.configreader.ConfigReader
import com.placelocalizer.main.facade.GooglePlacesFacade

/**
 * Main program Groovy script
 * Performs queries to Google Places API server
 * by specified coordinates and returns list of locations
 * found in JSON-format. Uses pre-defined radiuses with
 * binary search adjustment for best match of locations count
 *
 * @param latitude
 * @param longitude
 * @return JSON string
 */

/*
 * Output JSON format example:
 * {
    "status": "OK",
    "places": [
        {
            "lat": 13.531665,
            "id": "292cab03ad4da271e4fcdbaf7387597553173b4f",
            "icon": "http://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png",
            "lng": -2.4604145,
            "name": "West Africa"
        }
    ],
    "count": 1,
    "radius": 1000
}
 */

def application(args) {
    final String CONFIG_FILENAME = 'config.groovy'

    def cli = new CliBuilder(usage: 'placelocalizerelocalizer.groovy [latitude] [longitude]')
    cli.with {
        h longOpt: 'help', 'Show usage information'
    }
    def options = cli.parse(args)
    if (!options) {
        return
    }
    if (options.h) {
        cli.usage()
        return
    }
    def currentDir = new File(getClass().protectionDomain.codeSource.location.path).getParent()
    def configFile = new ConfigReader(new File(currentDir.toString() + '/' + CONFIG_FILENAME))
    def placesFacade = new GooglePlacesFacade(configFile)
    double lat = Double.parseDouble(options.arguments().get(0))
    double lng = Double.parseDouble(options.arguments().get(1))
    return placesFacade.getPlaces(lat, lng)
}

println(application(args))