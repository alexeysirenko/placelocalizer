package com.placelocalizer.main.exceptions

/**
 * EmptyResponseException class
 */
class EmptyResponseException extends Exception {
    EmptyResponseException(String message) {
        super(message)
    }
}
