package com.placelocalizer.main.exceptions

/**
 * FatalParseException class
 */
class FatalParseException extends ParseException {
    FatalParseException(String message) {
        super(message)
    }
}
