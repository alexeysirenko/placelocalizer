package com.placelocalizer.main.exceptions

/**
 * ParseException class
 */
class ParseException extends Exception {
    public ParseException(String message) {
        super(message)
    }
}
