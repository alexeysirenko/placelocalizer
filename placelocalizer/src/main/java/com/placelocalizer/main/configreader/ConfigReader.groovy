package com.placelocalizer.main.configreader


/**
 * Class which provides an interface to
 * script config object
 */
class ConfigReader {

    private final config

    /*
     * Class constructor specifying config file
     */
    public ConfigReader(File configFile) {
        assert configFile != null
        config = new ConfigSlurper().parse(configFile.text)
    }

    /*
     * Returns Google API key from configuration file
     * @return key as string
     */
    public String getKey() {
        String key = config.GOOGLE_KEY
        assert key != null && key.length() > 0
        return key
    }
    
    /*
     * Returns pre-configured radiuses array
     * @return array of integer radius
     */
    public int[] getRadiuses() {
        int[] radiuses = config.RADIUSES
        assert radiuses != null && radiuses.size() > 0
        return radiuses
    }

    /*
     * Returns pre-configured preferable output places count
     * @return Places count
     */
    public int getPlacesCount() {
        int count = config.PLACES_COUNT
        assert count > 0
        return count
    }
}
