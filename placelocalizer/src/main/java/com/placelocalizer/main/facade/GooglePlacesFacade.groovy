package com.placelocalizer.main.facade

import com.placelocalizer.main.configreader.ConfigReader
import com.placelocalizer.main.handlers.PickyPlacesController
import com.placelocalizer.main.json.JSONPlaces


/**
 * Main facade class which provides unified program interface
 */
class GooglePlacesFacade {
    private final PickyPlacesController controller

    /*
     * Class constructor specifying configuration object
     */
    public GooglePlacesFacade(ConfigReader config) {
        this.controller = new PickyPlacesController(config)
    }

    /*
     * Returns formatted JSON string of places found by provided
     * coordinates using Google Places API
     * @param lat Latitude
     * @param lng Longitude
     * @return Formatted JSON string
     */
    public String getPlaces(double lat, double lng) {
        def placesOutput = controller.getPlacesLookupOutput(lat, lng)
        def result = new JSONPlaces(placesOutput)
        return result.toString()
    }
}
