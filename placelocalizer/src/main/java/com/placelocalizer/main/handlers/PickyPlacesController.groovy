package com.placelocalizer.main.handlers

import com.placelocalizer.main.common.PlaceComparator
import com.placelocalizer.main.common.PlacesOutputObject
import com.placelocalizer.main.common.ParsePage
import com.placelocalizer.main.configreader.ConfigReader
import org.apache.log4j.Logger

/**
 * Main places search controller which provides
 * iterative radius adjustment of primary Google page results
 * depending of places found and post-loading of secondary Google
 * pages by nextPage token
 */
class PickyPlacesController {
    private final int[] RADIUSES
    private final DESIRED_PLACES_COUNT
    private final ParserController parserController
    private final TIMEOUT_WAITFORNEXTPAGE = 2000
    private final static Logger logger = Logger.getLogger(ParserController.class)

    /*
     * Class constructor specifying configuration object
     */
    public PickyPlacesController(ConfigReader config) {
        assert config != null
        RADIUSES = config.getRadiuses()
        DESIRED_PLACES_COUNT = config.getPlacesCount()
        parserController = new ParserController(config)
    }

    /*
     * Returns one final PlacesOutputObject object with status and places list
     * @param lat latitude
     * @param lng longitude
     * @return new PlacesOutputObject with list of places and status code included
     */
    public PlacesOutputObject getPlacesLookupOutput(double lat, double lng) {
        logger.debug(String.format('get places list at %s %s', lat, lng))
        assert RADIUSES.length > 0
        ParsePage resultPage = null
        // Enumerating array of radius to find one with satisfying places count in
        for (int i = 0; i < RADIUSES.length; i++) {
            assert RADIUSES[i] > 0
            resultPage = parserController.getFirstPage(lat, lng, RADIUSES[i])
            if (resultPage.size >= DESIRED_PLACES_COUNT) {
                // If necessary do binary adjustment of current radius
                if (i > 0 && resultPage.size > DESIRED_PLACES_COUNT) {
                    int radius1 = RADIUSES[i - 1]
                    int radius2 = RADIUSES[i]
                    resultPage = binarySearchAdjustPageRadius(resultPage, radius1, radius2, DESIRED_PLACES_COUNT)
                }
                break
            }
        }
        // Load secondary pages by token
        while (resultPage.nextPage != null) {
            sleep(TIMEOUT_WAITFORNEXTPAGE) // Need to wait until next page token will become relevant
            resultPage = parserController.getNextPage(resultPage)
        }
        return new PlacesOutputObject(resultPage)
    }

    /*
     * Provides radius adjustment for preciser match of desired places count
     * using binary search algorithm
     * @param page Initial previously found page with specified coordinates
     * @param radius1 lower bound radius
     * @param radius2 upper bound radius
     * @param desiredPlacesCount preferable places count to match
     * @return new adjusted ParsePage object
     */
    private ParsePage binarySearchAdjustPageRadius(ParsePage page, int radius1, int radius2, int desiredPlacesCount) {
        final STEP_COUNT = 3
        logger.debug(String.format('adjusting radius for %s r1=%d r2=%d', page, radius1, radius2))
        assert page != null
        ParsePage resultPage = page.clone()
        for (int i = 0; i < STEP_COUNT; i++) {
            assert radius2 > radius1
            int halfRadius = (radius2 + radius1) / 2
            resultPage = parserController.getFirstPage(page.lat, page.lng, halfRadius)
            if (resultPage.size == desiredPlacesCount || radius1 == radius2) {
                break
            } else if (resultPage.size < desiredPlacesCount) {
                radius1 = halfRadius
            } else {
                radius2 = halfRadius
            }
        }
        return resultPage
    }
}
