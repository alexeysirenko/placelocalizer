package com.placelocalizer.main.handlers

import com.placelocalizer.main.common.ParsePage
import com.placelocalizer.main.exceptions.EmptyResponseException
import com.placelocalizer.main.exceptions.FatalParseException
import com.placelocalizer.main.configreader.ConfigReader
import com.placelocalizer.main.json.JSONParser
import com.placelocalizer.main.web.GoogleRESTQuerier

//@Grab('log4j:log4j:1.2.17')
import org.apache.log4j.Logger

import java.text.ParseException

/**
 * Parser controller class which provides automatic
 * error handling and reconnection
 */
class ParserController {

    private final STATUS_OK = 'OK'
    private final STATUS_EMPTY = 'EMPTY'
    private final STATUS_ERROR = 'ERROR'
    private final JSONParser parser = new JSONParser()
    private final GoogleRESTQuerier googleRESTQuerier
    private final TIMEOUT_RECONNECT = 2000
    private final static Logger logger = Logger.getLogger(ParserController.class)

    /*
     * Class constructor specifying configuration object
     */
    public ParserController(ConfigReader config) {
        assert config != null
        this.googleRESTQuerier = new GoogleRESTQuerier(config.getKey())
    }

    /*
     * Returns single initial page result ParsePage object of parsing by provided coordinates and radius
     * @param lat latitude
     * @param lng longitude
     * @param radius radius
     * @return ParsePage object with places list and status code
     */
    public ParsePage getFirstPage(double lat, double lng, int radius) {
        logger.debug(String.format('loading primary page at %s %s with radius %s', lat, lng, radius))
        final int ATTEMPTS_COUNT = 3
        def resultPage = new ParsePage(lat, lng, radius)
        int i = 0
        while (i < ATTEMPTS_COUNT) {
            resultPage.nextPage = null
            resultPage.status = STATUS_ERROR
            try {
                resultPage.getPlaces().clear()
                def json = googleRESTQuerier.getPlacesJSON(lat, lng, radius)
                resultPage.nextPage = parser.parseJSON(json, resultPage.places)
                resultPage.status = STATUS_OK
                logger.debug(String.format('primary page loaded with status %s', resultPage.status))
                break
            } catch (EmptyResponseException e) {
                resultPage.status = STATUS_EMPTY
                logger.debug(e)
                break
            } catch (FatalParseException e) {
                logger.error(e)
                break
            } catch (ParseException | IOException e) {
                logger.error(e)
                if (i < ATTEMPTS_COUNT - 1) {
                    sleep(TIMEOUT_RECONNECT)  // Waiting
                }
            }
            i++
        }
        return resultPage
    }

    /*
     * Returns list of secondary pages for an existing page
     * @param page ParsePage with nextPage token set
     * @return ParsePage
     */
    public ParsePage getNextPage(ParsePage page) {
        logger.debug(String.format('loading secondary page for ', page))
        assert page != null && page.nextPage != null
        final int ATTEMPTS_COUNT = 3
        def resultPage = page.clone()
        int i = 0
        while (i < ATTEMPTS_COUNT) {
            resultPage.nextPage = null
            try {
                String json = googleRESTQuerier.getPlacesJSON(page.nextPage)
                resultPage.nextPage = parser.parseJSON(json, resultPage.places)
                logger.debug('secondary page loaded')
                break
            } catch (EmptyResponseException | FatalParseException e) {
                logger.error(e)
                break
            } catch (ParseException | IOException e) {
                logger.error(e)
                if (i < ATTEMPTS_COUNT - 1) {
                    sleep(TIMEOUT_RECONNECT)
                }
            }
            i++
        }
        return resultPage
    }
}
