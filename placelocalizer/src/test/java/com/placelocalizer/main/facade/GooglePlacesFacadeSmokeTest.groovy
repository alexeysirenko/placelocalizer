package com.placelocalizer.main.facade

import com.placelocalizer.main.configreader.ConfigReader
import groovy.json.JsonSlurper

/**
 * Created by oleksiy on 10.07.15.
 */
class GooglePlacesFacadeSmokeTest extends GroovyTestCase {
    void testGetPlaces() {
        final TEST_COUNT = 10
        final mostCity = new TestCase(48.466481, 35.050725, 41, ['Kharkivs\'ka St'])
        final dnipro = new TestCase(48.502343, 34.963397, 2, ['Amur-Nyzhn\'odniprovs\'kyi district'])
        final sura = new TestCase(48.330120, 35.120772, 1, ['Volos\'ke'])
        final afrika = new TestCase(17.177899, 13.404427, 1, ['West Africa'])
        List<TestCase> testCases = new ArrayList<>()
        testCases.add(mostCity)
        testCases.add(dnipro)
        testCases.add(sura)
        testCases.add(afrika)
        def facade = new GooglePlacesFacade(new ConfigReader(new File('src/main/java/com/placelocalizer/main/config.groovy')))
        def slurper = new JsonSlurper()
        for (int i = 0; i < TEST_COUNT; i++) {
           for (TestCase test: testCases) {
               def text = facade.getPlaces(test.getLat(), test.getLng())
               def object = slurper.parseText(text) // Most city
               assertTrue('Invalid JSON format', object instanceof Map)
               assertTrue('Invalid response status', object.status == 'OK')
               for (String placeName: test.getNames()) {
                   boolean contains = false
                   for (int j = 0; j < object.places.size(); j++) {
                       if (object.places[j].name == placeName) {
                           contains = true
                           break
                       }
                   }
                   assertTrue(String.format('Place %s is missing', placeName), contains)
               }
               assertTrue('Lack of places', object.places.size() == test.getCount())
               println(text)
           }
        }
    }

    private class TestCase {
        private final double mLat
        private final double mLng
        private final int mCount
        private final String[] mNames

        public TestCase(lat, lng, count, names) {
            mLat = lat
            mLng = lng
            mCount = count
            mNames = names
        }

        double getLat() {
            return mLat
        }

        double getLng() {
            return mLng
        }

        int getCount() {
            return mCount
        }

        String[] getNames() {
            return mNames
        }
    }
}
