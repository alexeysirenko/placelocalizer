package com.placelocalizer.main.facade

import com.placelocalizer.main.common.Place
import com.placelocalizer.main.configreader.ConfigReader
import groovy.json.JsonSlurper

/**
 * Created by oleksiy on 07.07.15.
 */
class GooglePlacesFacadeTest extends GroovyTestCase {
    void testGetPlaces() {
        def facade = new GooglePlacesFacade(new ConfigReader(new File('src/main/java/com/placelocalizer/main/config.groovy')))
        def slurper = new JsonSlurper()
        def object = slurper.parseText(facade.getPlaces(44.478025, -73.196475))
        assertTrue('Invalid JSON format', object instanceof Map)
        assertTrue('Invalid response status', object.status == 'OK')
        def vermont = false
        for (Place place: object.places) {
            if (place.getName() == 'University of Vermont') {
                vermont = true
                break
            }
        }
        assertTrue('University of Vermont is missing', vermont)
    }
}
